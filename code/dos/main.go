package main

import "strings"

// ChangeText - reemplazar el texto :uno, por la cadena ingresada
func ChangeText(text string) string {
	return strings.ReplaceAll(text, ":uno", "-2-")
}
