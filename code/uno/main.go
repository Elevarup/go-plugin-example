package main

import "strings"

func ChangeText(text string) string {
	return strings.ReplaceAll(text, ":uno", "-1-")
}
