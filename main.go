package main

import (
	"fmt"
	"log"
	"plugin"
)

func main() {
	function1 := getFunction("plugins/uno.so")
	function2 := getFunction("plugins/dos.so")

	text := `:uno :uno :uno :uno :uno :uno :uno :uno`
	//EXECUTE
	fmt.Println("--- RESULTADO ---")
	fmt.Println("1- Execute function: ", function1(text))
	fmt.Println("2- Execute function: ", function2(text))
}

func getFunction(path string) func(string) string {

	//OPEN PLUGIN
	p, err := plugin.Open(path)
	if err != nil {
		log.Fatalln("plugin.Open, error:", err)
	}
	//LOOKUP VAR OR FUNCTION
	sym, err := p.Lookup("ChangeText")
	if err != nil {
		log.Fatalln("plugin, Lookup, error: ", err)
	}
	function := sym.(func(string) string)

	// RETURN VAR OR FUNCTION
	return function
}
