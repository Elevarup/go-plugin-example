OBJS  := plugins/uno.so plugins/dos.so
EXEC  := bin/exec
BUILD := go build -o


all: $(EXEC)
	./$<

# $@ -->> nombre del objetivo
# $< -->> nombre de la primera dependencia

# Generar un plugin en golang
# comando ejemplo1 (sin especificar una ruta):
# go build -buildmode=plugin
# comando ejemplo2 (especificando la ruta y el nombre del plugin):
# go build -buildmode=plugin -o plugins/uno.so

plugins/%.so: code/%/main.go
	$(BUILD) $@ -buildmode=plugin $<

$(EXEC): main.go $(OBJS)
	$(BUILD) $@ .

clean:
	rm -rf plugins/* bin
