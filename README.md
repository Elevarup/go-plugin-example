# go-plugin-example

execute: `make`

clean: `make clean`

# Dependencias

Make: https://www.gnu.org/software/make/


# Ejecución del proyecto

Escribir en la línea de comandos ( la terminal o la consola), el siguiente comando:

``make``

Este generará la compilación de los plugins y del proyecto, además de ejecutarlo.

# ¿Qué es un plugin en golang?
Es un paquete que se compila con el siguiente commando:

``go build -buildmode=plugin``

El comando generar un fichero con extensión `.so`

# ¿ Cómo se utiliza un plugin generado por golang en golang?

Existe un paquete estandar de golang, que nos facilita el uso de plugins. 

``import "plugin"``

El uso de un plugin consta de los siguientes pasos:

1. Abrir el fichero con extensión `.so`
    
    ``p, err := plugin.Open("path/to/plugin/fileName.so")``

2. Obtener la variable o la función de del plugin abierto.

    ``sym, err := p.Lookup("nombreVariableOFuncion")``

3. Convertir la variable `sym` al tipo de dato con el que se construyó en el plugin.

    ``function, ok := sym.(func(string) string)``

Ejemplo:

Funcion del plugin
```
func ChangeText(text string) string {
	return strings.ReplaceAll(text, ":uno", "-2-")
}
```


Obtenera la función del plugin, gracias al uso del paquete estandar `"plugin"`
```
func getFunction(path string) func(string) string {

	//OPEN PLUGIN
	p, err := plugin.Open(path)
	if err != nil {
		log.Fatalln("plugin.Open, error:", err)
	}
	//LOOKUP VAR OR FUNCTION
	sym, err := p.Lookup("ChangeText")
	if err != nil {
		log.Fatalln("plugin, Lookup, error: ", err)
	}
	function := sym.(func(string) string)

	// RETURN VAR OR FUNCTION
	return function
}

```

Utilización de la función obtenida del plugin
```
func main() {
	function1 := getFunction("plugins/uno.so")
	function2 := getFunction("plugins/dos.so")

	text := `:uno :uno :uno :uno :uno :uno :uno :uno`
	//EXECUTE
	fmt.Println("--- RESULTADO ---")
	fmt.Println("1- Execute function: ", function1(text))
	fmt.Println("2- Execute function: ", function2(text))
}
```

Resultado en la terminal de comando

```
--- RESULTADO ---
1- Execute function:  -1- -1- -1- -1- -1- -1- -1- -1-
2- Execute function:  -2- -2- -2- -2- -2- -2- -2- -2-

```

# Referencias

1. https://pkg.go.dev/plugin 
2. https://github.com/eliben/code-for-blog/tree/master/2021/go-htmlize-plugin
3. https://eli.thegreenplace.net/2021/plugins-in-go/
